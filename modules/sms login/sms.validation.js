
/**
 * Define every validation requires
 * to perform CRUD on category.
 */


import Joi from 'joi';

// Defining base schema.
const articleSchema = {
  mobile: Joi.string(),
};


export default {
  // adding middleware level validation on api endpoint
  add: {
    body: {
      mobile: articleSchema.mobile.required(),
    },
  },
};
