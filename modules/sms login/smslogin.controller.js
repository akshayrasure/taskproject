

// const passport = require('passport');
import jwt from 'jwt-simple';
import db from '../user/user.model';
// import sms from '../sms/strategy/sms';
import response from './sms.response';
// import verifier from '../middleware/verifier';
import config from '../../config/database';

const signIn = [
  (req, res, next) => {
    const mobileNum = req.body.mobile;

    // if(db.find({ mobile: mobileNum }))
    db.findOneAndUpdate({ mobile: mobileNum }, { $setOnInsert: { mobile: mobileNum, role: 'user' } },
      { upsert: true, new: true }).then((user) => { //eslint-disable-line
      // const otp = Math.floor(Math.random() * 90000) + 10000;
        console.log(user);
        db.addOtp(user, 12345).then(() => {
            // const mobiles = req.body.mobile;
            // sms.send(mobiles, `Your OTP is ${otp}`).then(() => {
          response.ok(res, { message: 'Your OTP has been sent', userId: user._id });
            // });
        })
            .catch((error) => {
              next(error);
            });
      }).catch((error) => {
        next(error);
      });
  },
];

const otpVerification = [
  (req, res, next) => {
    const otp = req.body.otp;
    const userId = req.body.userId;
    if (!otp || !userId) {
      return response.badrequest(res, { error: 'OTP & user is mendetory' });
    }
    db.findUserById(userId)
      .then((user) => {
        if (user === null) {
          return response.badrequest(res, { error: 'No user Found' });
        }
        if (user.otp === otp) {
          const token = jwt.encode(user, config.secret);
          const defaultOtp = null;
          db.addOtp(user, defaultOtp);
          return response.ok(res, { message: 'Logged in successfully', data: user, token });
          // return db.tokens.findTokenByUser(userId).then(token => res.status(200).send({
          //   message: 'Logged in successfully',
          //   token,
          //   role : user.role,
          //   society : user.society,
          //   user,
          //   signin: true,
          // }));
        }
        return response.badrequest(res, { error: 'Invalid OTP' });
      })
      .catch((error) => {
        next(error);
      });
  },
];

const resendOtp = [
  (req, res, next) => {
    const userId = req.body.userId;
    db.findUserById(userId)
      // .then((user) => { // eslint-disable-line

      // })
      .then((user) => {
        if (!user) {
          return response.badrequest(res, { error: 'Invalid user' });
        }
        // const otp = Math.floor(Math.random() * 90000) + 10000;
        db.addOtp(user, 12345);
        // sms.send(user.mobile, `Your OTP is ${otp}`)
          // .then(() =>
        response.ok(res, { message: 'Your OTP has been sent', userId: user._id });
          // );
      })
      .catch((error) => {
        next(error);
      });
  },
];

export default {
  signIn,
  otpVerification,
  resendOtp,
};
