/**
 * Define your module's express routes here.
 */

import express from 'express';
import validator from 'express-validation';
import validation from './sms.validation';
import controller from '../sms login/smslogin.controller';

const routes = express.Router(); // eslint-disable-line new-cap

// ** post api === /api/user
// routes.post('/', controller);
/**
 * @api {post} /api/smslogin Login.
 * @apiGroup Login
 * @apiPermission user/admin
 * @apiVersion 0.0.1
 *
 * @apiExample Api Url:
 * http://localhost/api/smslogin
 *
 * @apiParam {String} mobile mobile of user (should be unique).
 *
 * @apiParamExample {json} Body.
 * {
 "mobile": "9967520490",
}

 * @apiSuccess {String} userId Users unique ID.
 * @apiSuccessExample {json} Success.
 *
 {
    "statusCode": 200,
    "message": "Your OTP has been sent",
    "userId": "5a91062a356bc61114e6a471"
}

* @apiError {String} user user not exists
 * @apiErrorExample {json} Error: 400
 * HTTP/1.1 400
 *{
    "statusCode": 400,
    "message": "user not exists, please contact to Admin "
}
 */
// ** get api === /api/user
routes.post('/',
validator(validation.add),
controller.signIn);

/**
 * @api {post} /api/smslogin/verifyotp Verifyotp.
 * @apiGroup Login
 * @apiPermission admin/user
 * @apiVersion 0.0.1
 *
 * @apiExample Api Url:
 * http://localhost/api/smslogin/verifyotp
 *
 * @apiParam {String} userId id Users unique ID.
 * @apiParam {String} otp otp.
 * @apiParamExample {json} Body.
 * {

    "userId": "5a91062a356bc61114e6a471",
    "otp":"68935"
}
 * @apiSuccess {String} userId Users unique ID.
 * @apiSuccess {String} name Name Of The User
 * @apiSuccess {String} mobile User Mobile Number
 * @apiSuccess {String} role User Role administrator/user
 * @apiSuccess {String} mobile User Mobile Number
 * @apiSuccess {String} username User Name(axxxxxx@gmail.com)
 * @apiSuccess {String} password User Password
 * @apiSuccess {String} token token.
 * @apiSuccessExample {json} Success.
 *
 {
    "statusCode": 200,
    "message": "Logged in successfully",
    "data": {
        "_id": "5a91062a356bc61114e6a471",
        "name": "bhavesh raval",
        "mobile": "9967520490",
        "role": "admin",
        "__v": 0,
        "otp": "68935",
        "updatedAt": "2018-03-21T07:03:22.541Z",
        "createdBy": "5a91212fc26ab6217c729612",
        "credentials": {
            "password": "123456789",
            "username": "bhavesh97@gmail.com"
        }
    },
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1YTkxMDYyYTM1NmJjNjExMTRlNmE0NzEiLCJuYW1lIjoiYmhhdmVzaCByYXZhbCIsIm1vYmlsZSI6Ijk5Njc1MjA0OTAiLCJyb2xlIjoiYWRtaW4iLCJfX3YiOjAsIm90cCI6IjY4OTM1IiwidXBkYXRlZEF0IjoiMjAxOC0wMy0yMVQwNzowMzoyMi41NDFaIiwiY3JlYXRlZEJ5IjoiNWE5MTIxMmZjMjZhYjYyMTdjNzI5NjEyIiwiY3JlZGVudGlhbHMiOnsicGFzc3dvcmQiOiIxMjM0NTY3ODkiLCJ1c2VybmFtZSI6ImJoYXZlc2g5N0BnbWFpbC5jb20ifX0.S0PHZStRKmE2C5mr3IyoUZGEJUkHywEBaLBk0IgaR2o"
}

* @apiError {String} otpvalidation Invalid OTP
 * @apiErrorExample {json} Error: 400
 * HTTP/1.1 400
 *{
    "statusCode": 400,
    "error": "Invalid OTP"
}
 */
// ** get api === /api/user
routes.post('/verifyotp', controller.otpVerification);

/**
 * @api {post} /api/smslogin/resendotp Resendotp.
 * @apiGroup Login
 * @apiPermission admin/user
 * @apiVersion 0.0.1
 *
 * @apiExample Api Url:
 * http://localhost/api/smslogin/resendotp
 *
 * @apiParam {String} userId id Users unique ID.
 * @apiParamExample {json} Body.
 * {

    "userId": "5a91062a356bc61114e6a471"
}
 * @apiSuccess {String} userId Users unique ID.
 * @apiSuccessExample {json} Success.
 *{
    "statusCode": 200,
    "message": "Your OTP has been sent",
    "userId": "5a91062a356bc61114e6a471"
}
* @apiError {String} uservalidation Invalid user
 * @apiErrorExample {json} Error: 400
 * HTTP/1.1 400
 *{
    "statusCode": 400,
    "error": "Invalid user"
}
 */

// ** get api === /api/user
routes.post('/resendotp', controller.resendOtp);
// // ** get api === /api/user/:_id
// routes.get('/:_id', controller.getById);

// // ** put api === /api/user/:_id
// routes.put('/:_id', controller.updateById);

// // ** delete api === /api/user/:_id
// routes.delete('/:_id', controller.removeById);

export default routes;
