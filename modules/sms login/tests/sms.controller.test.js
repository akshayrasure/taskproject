import chai, { expect } from 'chai';
import sinon from 'sinon';
// import proxyquire from 'proxyquire';
import smsloginController from '../smslogin.controller';

// stubbing category dependency from category.controller.
// const smsloginController = proxyquire('../category.controller', { category: categoryModel })

let sandbox;
beforeEach(() => {
  sandbox = sinon.sandbox.create();
});

afterEach(() => {
  sandbox.restore();
});

describe('### SMS  Login controller **smslogin.controller.js**', () => {
  it('should have signin property', () => {
    expect(smsloginController).to.be.a('object');
    expect(smsloginController.signIn).to.be.a('array');
  });
  it('should have otpVerification property', () => {
    expect(smsloginController).to.be.a('object');
    expect(smsloginController.otpVerification).to.be.a('array');
  });
  it('should have resendOtp property', () => {
    expect(smsloginController).to.be.a('object');
    expect(smsloginController.resendOtp).to.be.a('array');
  });
});
