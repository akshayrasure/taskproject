import chai, { expect } from 'chai';
import index from '../smslogin.index';

chai.config.includeStack = true;

describe('### sms login module **index.js**', () => {
  it('should has controller, route property', () => {
    expect(index).to.have.keys('controller', 'route');
  });
});
