// import jwt from 'jwt-simple';
// import mongoose from 'mongoose';
// import request from 'supertest-as-promised';
// import httpStatus from 'http-status';
// import chai, { expect } from 'chai';
// import config from '../../../config/database';
// import app from '../../index';
// import User from '../../user/user.model';
// // import request from 'supertest-as-promised';
// // import httpStatus from 'http-status';
// // import chai, { expect } from 'chai';
// // import app from '../../../index';
// // import config from '../../../config/database';

// // import userFixture from '../helpers/user.fixtures';
// // chai.config.includeStack = true;

// chai.config.includeStack = true;

// /**
//  * root level hooks
//  */
// after((done) => {
//   // required because https://github.com/Automattic/mongoose/issues/1251#issuecomment-65793092
//   mongoose.models = {};
//   mongoose.modelSchemas = {};
//   mongoose.connection.db.dropDatabase();
//   mongoose.connection.close();
//   done();
// });

// const userData = {
//   _id: '5ab0d48271402e317a64419a',
//   name: 'bhavesh',
//   mobile: '9967520490',
//   role: 'admin',
//   updatedAt: '2018-03-21T10:11:19.751Z',
//   otp: '74322',
//   credentials: {
//     password: '123456789',
//     username: 'abc@fortunekit.com',
//   },
// };

// let token;

// beforeEach(() => {
//   token = jwt.sign(
//             JSON.parse(JSON.stringify(userData)),
//             config.jwtSecret,
//             { expiresIn: '1 day' });
// });

// describe('## User APIs', () => {
//   let user = ({
//     name: 'KK123',
//     mobile: '8805664550',
//   },
//   {
//     name: 'suresh',
//     mobile: '8805448221',
//   });

//   describe('# POST /api/users', () => {
//     it('should create a new user', (done) => {
//       request(app)
//         .post('/api/users')
//         .send(user)
//         .set('Authorization', `Bearer ${token}`)
//         .expect(httpStatus.OK)
//         .then((res) => {
//           expect(res.body.name).to.equal(user.name);
//           expect(res.body.mobile).to.equal(user.mobile);
//           user = res.body;
//           done();
//         })
//         .catch(done);
//     });
//   });

//   describe('# GET /api/users/:userId', () => {
//     it('should get user details', (done) => {
//       request(app)
//         .get(`/api/users/${user._id}`)
//         .set('Authorization', `Bearer ${token}`)
//         .expect(httpStatus.OK)
//         .then((res) => {
//           expect(res.body.name).to.equal(user.name);
//           expect(res.body.mobile).to.equal(user.mobile);
//           done();
//         })
//         .catch(done);
//     });

//     it('should report error with message - Not found, when user does not exists', (done) => {
//       request(app)
//         .get('/api/users/56c787ccc67fc16ccc1a5e92')
//         .set('Authorization', `Bearer ${token}`)
//         .expect(httpStatus.NOT_FOUND)
//         .then((res) => {
//           expect(res.body.message).to.equal('user is not found');
//           done();
//         })
//         .catch(done);
//     });
//   });

//   describe('# PUT /api/users/:userId', () => {
//     it('should update user details', (done) => {
//       user.name = 'KK';
//       request(app)
//         .put(`/api/users/${user._id}`)
//         .set('Authorization', `Bearer ${token}`)
//         .send(user)
//         .expect(httpStatus.OK)
//         .then((res) => {
//           expect(res.body.name).to.equal('KK');
//           expect(res.body.mobile).to.equal(user.mobile);
//           done();
//         })
//         .catch(done);
//     });
//   });

//   describe('# GET /api/users/', () => {
//     it('should get all users', (done) => {
//       request(app)
//         .get('/api/users')
//         .set('Authorization', `Bearer ${token}`)
//         .expect(httpStatus.OK)
//         .then((res) => {
//           expect(res.body).to.be.an('array');
//           done();
//         })
//         .catch(done);
//     });

//     it('should get all users (with limit and skip)', (done) => {
//       request(app)
//         .get('/api/users')
//         .set('Authorization', `Bearer ${token}`)
//         .query({ limit: 10, skip: 1 })
//         .expect(httpStatus.OK)
//         .then((res) => {
//           expect(res.body).to.be.an('array');
//           done();
//         })
//         .catch(done);
//     });
//   });

//   describe('# DELETE /api/users/', () => {
//     it('should delete user', (done) => {
//       request(app)
//         .delete(`/api/users/${user._id}`)
//         .set('Authorization', `Bearer ${token}`)
//         .expect(httpStatus.OK)
//         .then((res) => {
//           expect(res.body.name).to.equal('KK');
//           expect(res.body.mobile).to.equal(user.mobile);
//           done();
//         })
//         .catch(done);
//     });
//   });

//   describe('# GET /api/users/search', () => {
//     it('should get users searched by string', (done) => {
//       request(app)
//         .get('/api/users/search')
//         .set('Authorization', `Bearer ${token}`)
//         .query({ search: 'vi' })
//         .expect(httpStatus.OK)
//         .then((res) => {
//           expect(res.body).to.be.an('object');
//           expect(res.body.results).to.be.an('array');
//           expect(res.body.results[0]).to.have.property('name');
//           expect(res.body.results[0]).to.have.property('mobile');
//           expect(res.body.results[0]).to.have.property('role');
//           expect(res.body.results[0]).to.have.property('createdAt');
//           expect(res.body.results[0]).to.have.property('updatedAt');
//           done();
//         })
//         .catch(done);
//     });

//     it('should get users searched by number', (done) => {
//       request(app)
//         .get('/api/users/search')
//         .set('Authorization', `Bearer ${token}`)
//         .query({ search: '87' })
//         .expect(httpStatus.OK)
//         .then((res) => {
//           expect(res.body).to.be.an('object');
//           expect(res.body.results).to.be.an('array');
//           expect(res.body.results[0]).to.have.property('name');
//           expect(res.body.results[0]).to.have.property('mobile');
//           expect(res.body.results[0]).to.have.property('role');
//           expect(res.body.results[0]).to.have.property('createdAt');
//           expect(res.body.results[0]).to.have.property('updatedAt');
//           done();
//         })
//         .catch(done);
//     });

//     it('should get users and cursor pagination properties', (done) => {
//       request(app)
//         .get('/api/users/search')
//         .set('Authorization', `Bearer ${token}`)
//         .query({ search: '88' })
//         .expect(httpStatus.OK)
//         .then((res) => {
//           expect(res.body).to.have.property('previous');
//           expect(res.body).to.have.property('hasPrevious');
//           expect(res.body).to.have.property('next');
//           expect(res.body).to.have.property('hasNext');
//           expect(res.body.hasPrevious).to.be.an('boolean');
//           expect(res.body.hasNext).to.be.an('boolean');
//           // this test is written for result having limit 1.
//           expect(res.body.next).to.be.an('string');
//           expect(res.body.previous).to.be.an('string');
//           // done();

//           return request(app)
//           .get('/api/users/search')
//           .set('Authorization', `Bearer ${token}`)
//           .query({ search: '88', next: res.body.next })
//           .expect(httpStatus.OK);
//         })
//         .then((res) => {
//           expect(res.body);
//           // done();

//           return request(app)
//           .get('/api/users/search')
//           .set('Authorization', `Bearer ${token}`)
//           .query({ search: '88', previous: res.body.previous })
//           .expect(httpStatus.OK);
//         })
//         .then((res) => {
//           expect(res.body);
//           done();
//         })
//         .catch(done);
//     });
//   });
// });

// // import mongoose from 'mongoose';
// // import jwt from 'jwt-simple';
// // import request from 'supertest-as-promised';
// // import httpStatus from 'http-status';
// // import chai, { expect } from 'chai';
// // import app from '../../../index';
// // import config from '../../../config/database';
// // import User from '../../user/user.model';
// // // import userFixture from '../helpers/user.fixtures';
// // chai.config.includeStack = true;

// // // after((done) => {
// // //     // required because https://github.com/Automattic/mongoose/issues/1251#issuecomment-65793092
// // //   mongoose.models = {};
// // //   mongoose.modelSchemas = {};
// // //   mongoose.connection.db.dropDatabase();
// // //   mongoose.connection.close();
// // //   done();
// // // });
// // after((done) => {
// //   mongoose.models = {};
// //   mongoose.modelSchemas = {};
// //   mongoose.connection.close();
// //   done();
// // });
// // const userData = {
// //   _id: '5ab0d48271402e317a64419a',
// //   name: 'bhavesh',
// //   mobile: '9967520490',
// //   role: 'admin',
// //   updatedAt: '2018-03-21T10:11:19.751Z',
// //   otp: '74322',
// //   credentials: {
// //     password: '123456789',
// //     username: 'abc@fortunekit.com',
// //   },
// // };

// // const userData1 = {
// //   name: 'bhavesh',
// //   mobile: '9967520491',
// //   role: 'admin',
// //   otp: '74322',
// //   credentials: {
// //     password: '123456789',
// //     username: 'abc@fortunekit.com',
// //   },
// // };

// // let token;

// // // inserting data from fixtures into database.
// // beforeEach((done) => {
// //   token = jwt.encofode(userData, config.secret);
// //   User.collection.insert(userData1, done);
// // });

// //   // emptying database
// // afterEach((done) => {
// //   User.remove({}, () => {
// //     done();
// //   });
// // });

// // describe('## User APIs (integration test)', () => {
// //   const user = { _id: '5ab0d48271402e317a64419b',
// //     name: 'bhavesh',
// //     mobile: '9967520491',
// //     role: 'admin',
// //     otp: '74322',
// //     credentials: {
// //       password: '123456789',
// //       username: 'abcd@fortunekit.com',
// //     },
// //   };
// // // post
// //   describe('# POST /api/user', () => {
// //   //  console.log(userData, user, token);
// //     it('should not create a user as token not provided', (done) => {
// //       request(app)
// //         .post('/api/user')
// //         .send(user)
// //         .expect(httpStatus.UNAUTHORIZED)
// //         .then((res) => {
// //           expect(res.body);
// //           done();
// //         })
// //         .catch(done);
// //     });

// //     // it('should  create a user', (done) => {
// //     //   request(app)
// //     //     .post('/api/user')
// //     //     .set('Authorization', `jwt ${token}`)
// //     //     .send(user)
// //     //     .expect(httpStatus.OK)
// //     //     .then((res) => {
// //     //       expect(res.body);
// //     //       expect(res.body.data).to.have.property('name');
// //     //       expect(res.body.data).to.have.property('mobile');
// //     //       expect(res.body.data).to.have.property('role');
// //     //       expect(res.body.data.name).to.equal(user.name);
// //     //       expect(res.body.data.mobile).to.equal(user.mobile);
// //     //       expect(res.body.data.role).to.equal(user.role);
// //     //       done();
// //     //     })
// //     //     .catch(done);
// //     // });
// //   });
// // // get list
// //   describe('# GET /api/user', () => {
// //     it('should not get user list as token not provided', (done) => {
// //       request(app)
// //         .get('/api/user')
// //         .expect(httpStatus.UNAUTHORIZED)
// //         .then((res) => {
// //           expect(res.body);
// //           done();
// //         })
// //         .catch(done);
// //     });

// //     // it('should get user list', (done) => {
// //     //   request(app)
// //     //     .get('/api/user')
// //     //     .set('Authorization', `jwt ${token}`)
// //     //     .expect(httpStatus.OK)
// //     //     .then((res) => {
// //     //       expect(res.body).to.be.an('object');
// //     //       expect(res.body.statusCode).to.equal(200);
// //     //       expect(res.body.message).to.equal('Data found');
// //     //       expect(res.body.data).to.be.an('array');
// //     //       expect(res.body.data[0]).to.have.property('name');
// //     //       expect(res.body.data[0]).to.have.property('mobile');
// //     //       expect(res.body.data[0]).to.have.property('role');
// //     //       expect(res.body.data[1].name).to.equal('anand');
// //     //       done();
// //     //     })
// //     //     .catch(done);
// //     // });
// //   });
// // // get by id
// //   describe('# GET BY ID /api/user/:id', () => {
// //     it('should not get user as token not provided', (done) => {
// //       request(app)
// //         .get('/api/user/4ed2b809d7446b9a0e000023')
// //         .expect(httpStatus.UNAUTHORIZED)
// //         .then((res) => {
// //           expect(res.body);
// //           done();
// //         })
// //         .catch(done);
// //     });

// //     // it('should get user by id', (done) => {
// //     //   request(app)
// //     //     .get('/api/user/4ed2b809d7446b9a0e000023')
// //     //     .set('Authorization', `jwt ${token}`)
// //     //     .expect(httpStatus.OK)
// //     //     .then((res) => {
// //     //       expect(res.body).to.be.an('object');
// //     //       expect(res.body.data).to.be.an('object');
// //     //       expect(res.body.statusCode).to.equal(200);
// //     //       expect(res.body.message).to.equal('Data found');
// //     //       expect(res.body.data).to.have.property('name');
// //     //       expect(res.body.data).to.have.property('mobile');
// //     //       expect(res.body.data).to.have.property('role');
// //     //       expect(res.body.data.name).to.equal('anand');
// //     //       done();
// //     //     })
// //     //     .catch(done);
// //     // });
// //   });
// // // update
// //   describe('# UPDATE BY ID /api/user/:id', () => {
// //     it('should not update user as token not provided', (done) => {
// //       request(app)
// //         .put('/api/user/4ed2b809d7446b9a0e000023')
// //         .expect(httpStatus.UNAUTHORIZED)
// //         .then((res) => {
// //           expect(res.body);
// //           done();
// //         })
// //         .catch(done);
// //     });

// //     // it('should get user by id', (done) => {
// //     //   request(app)
// //     //     .put('/api/user/4ed2b809d7446b9a0e000023')
// //     //     .send(user)
// //     //     .set('Authorization', `jwt ${token}`)
// //     //     .expect(httpStatus.OK)
// //     //     .then((res) => {
// //     //       expect(res.body).to.be.an('object');
// //     //       expect(res.body.data).to.be.an('object');
// //     //       expect(res.body.statusCode).to.equal(200);
// //     //       expect(res.body.message).to.equal('user updated');
// //     //       expect(res.body.data).to.have.property('name');
// //     //       expect(res.body.data).to.have.property('mobile');
// //     //       expect(res.body.data).to.have.property('role');
// //     //       expect(res.body.data.name).to.equal(user.name);
// //     //       done();
// //     //     })
// //     //     .catch(done);
// //     // });
// //   });
// // // delete
// //   describe('# DELETE BY ID /api/user/:id', () => {
// //     it('should not delete user as token not provided', (done) => {
// //       request(app)
// //         .delete('/api/user/4ed2b809d7446b9a0e000023')
// //         .expect(httpStatus.UNAUTHORIZED)
// //         .then((res) => {
// //           expect(res.body);
// //           done();
// //         })
// //         .catch(done);
// //     });

// //     // it('should delete user by id', (done) => {
// //     //   request(app)
// //     //     .delete('/api/user/4ed2b809d7446b9a0e000023')
// //     //     .set('Authorization', `jwt ${token}`)
// //     //     .expect(httpStatus.OK)
// //     //     .then((res) => {
// //     //       expect(res.body).to.be.an('object');
// //     //       expect(res.body.data).to.be.an('object');
// //     //       expect(res.body.statusCode).to.equal(200);
// //     //       expect(res.body.message).to.equal('user deleted');
// //     //       expect(res.body.data).to.have.property('name');
// //     //       expect(res.body.data).to.have.property('mobile');
// //     //       expect(res.body.data).to.have.property('role');
// //     //       expect(res.body.data.name).to.equal('anand');
// //     //       done();
// //     //     })
// //     //     .catch(done);
// //     // });
// //   });
// // });
