import express from 'express';
import authController from './auth.controller';

const route = express.Router();// eslint-disable-line new-cap

/**
 * @api {post} /api/login get token.
 * @apiVersion 0.0.1
 * @apiGroup Authentication
 *
 * @apiParam {String} mobile registered mobile number.
 * @apiParam {String} password password.
 *
 * @apiParamExample {json} Body.
 * {
"mobile":"9967520490",
"password":"12345"
}

 * @apiSuccess {String} mobile mobile number.
 * @apiSuccess {String} password password.
 *
 * @apiSuccessExample {json} Success.
 *
 *    {
    "statusCode": 200,
    "success": true,
    "token": "JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQi
    OiI1YTk2NmVhYzZlYTBhMTIwNjA2YjM3NTYiLCJ1cGRhdGVkQXQiOiIyMDE
    4LTAyLTI4VDA4OjU2OjEyLjE1M1oiLCJjcmVhdGVkQXQiOiIyMDE4LTAyLTI
    4VDA4OjU2OjEyLjE1M1oiLCJuYW1lIjoiYmhhdmVzaCIsIm1vYmlsZSI6Ijk
    5Njc1MjA0OTAiLCJwYXNzd29yZCI6IiQyYSQxMCRITnZvNldoTjAyR29QVUF
    FV1ZlZzN1bFU4TXlyell5L3RMUTJBQ29nTzBPTURGbFY3aWIuaSIsInJvbGU
    iOiJhZG1pbiIsIl9fdiI6MH0.32I-KxYNkHOMIZCh3CF6s2F5D_IQWZUx5gTgH6GI2Q4",
    "user": {
        "_id": "5a966eac6ea0a120606b3756",
        "updatedAt": "2018-02-28T08:56:12.153Z",
        "createdAt": "2018-02-28T08:56:12.153Z",
        "name": "bhavesh",
        "mobile": "9967520490",
        "password": "$2a$10$HNvo6WhN02GoPUAEWVeg3ulU8MyrzYy/tLQ2ACogO0OMDFlV7ib.i",
        "role": "admin",
        "__v": 0
    }
}
 *
 * @apiError {String} mobile mobile number not registered.
 * @apiErrorExample {json} Error.
 * HTTP/1.1 404 NOT FOUND
 * {
    "statusCode": 404,
    "success": false,
    "message": "Authentication failed. User not found."
}
 *
 * @apiError {String} password wrong password.
 * @apiErrorExample {json} Error.
 * HTTP/1.1 401 UNAUTHORIZED
 * {
    "statusCode": 401,
    "success": false,
    "message": "Authentication failed. Wrong password."
}
 */
route.post('/', authController.authenticateUser);


export default route;
