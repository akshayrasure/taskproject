import controller from './auth.controller';
import route from './auth.route';
import response from './auth.response';

export default {
  controller,
  route,
  response,
};
