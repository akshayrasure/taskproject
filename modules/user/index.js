
/**
 * export all file to become consumable.
 * controller: controller of the module
 * model: model of the module
 * response: response of the module
 * route: route of the module
 * validation: validation of the module
 */


import controller from './user.controller';
import model from './user.model';
import response from './user.response';
import route from './user.route';
import validation from './user.validation';

export default {
  controller,
  model,
  response,
  route,
  validation,
};
