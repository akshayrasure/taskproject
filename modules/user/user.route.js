/**
 * Define your module's express routes here.
 */
import passport from 'passport';
import express from 'express';
import controller from './user.controller';

const routes = express.Router(); // eslint-disable-line new-cap

routes.post('/',
 passport.authenticate('jwt', { session: false }),
//  , validator(validation.add),
//  accesscontrol.accessControlMiddleware.check({
//    resource: 'user1',
//    action: 'create',
//  }),
 controller.add);

routes.get('/',
passport.authenticate('jwt', { session: false }),
// accesscontrol.accessControlMiddleware.check({
//   resource: 'user1',
//   action: 'read',
// }),
 controller.get);

routes.get('/:_id',
passport.authenticate('jwt', { session: false }),
//  accesscontrol.accessControlMiddleware.check({
//   resource: 'user1',
//   action: 'read',
// }),
 controller.getById);

routes.put('/:_id', 
passport.authenticate('jwt', { session: false }),
//  accesscontrol.accessControlMiddleware.check({
//   resource: 'user1',
//   action: 'update',
// }),
 controller.updateById);

routes.delete('/:_id', 
passport.authenticate('jwt', { session: false }),
//  accesscontrol.accessControlMiddleware.check({
//   resource: 'user1',
//   action: 'delete',
// }),
 controller.removeById);

export default routes;
