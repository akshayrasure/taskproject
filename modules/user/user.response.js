
/**
 * Define every responses which we going to
 * use while building product CRUD.
 */


import Boom from 'boom';
import httpStatus from 'http-status';
import Promise from 'bluebird';

export default {

  ERROR: {
    /**
     * server error function.
     * @param {Error} error instance of Error.
     * @returns {Error} instance of Boom error
     */
    serverError(error) {
      return Promise.reject(Boom.internal(error.message));
    },
  /**
     * Check for mongoose validation error
     * if validation error send validation error
     * else server error.
     * @param {Error} error mongoose error object
     * @returns {Error} instance of Boom error.
     */
    validateError(error) {
      if (error.name === 'ValidationError') {
        //
        const errors = Object.keys(error.errors).map((key) => {
          if (error.errors[key] && error.errors[key].kind === 'unique') {
            error.errors[key].code = 409; // eslint-disable-line no-param-reassign
          } else if (error.errors[key]) {
            error.errors[key].code = 400; // eslint-disable-line no-param-reassign
          }
          error.errors[key].field = error.errors[key].path; // eslint-disable-line no-param-reassign
          return error.errors[key];
        });
        return Promise.reject(Boom.badRequest('Validation error', { errors }));
      }
      return Promise.reject(Boom.internal(error.message));
    },
  },
  /**
   * successful response
   * @param {Object<response>} res response object of express.js
   * @param {Object} options options.
   * @param {String} options.message message to be pass.
   * @param {Any}   options.data data be send.
   */

  ok: (res, options = {}) => {
    const opts = Object.assign({ statusCode: httpStatus.OK }, options);
    return res.status(httpStatus.OK).send(opts);
  },

  /**
  * successful creation
  * @param {Object<response>} res response object of express.js
  * @param {Object} options options.
  * @param {String} options.message message to be pass.
  * @param {Any}   options.data data be send.
  */

  created: (res, options = {}) => {
    const opts = Object.assign({ statusCode: httpStatus.CREATED }, options);
    return res.status(httpStatus.CREATED).send(opts);
  },

  /**
  * successful creation
  * @param {Object<response>} res response object of express.js
  * @param {Object} options options.
  * @param {String} options.message message to be pass.
  * @param {Any}   options.data data be send.
  */

  badrequest: (res, options = {}) => {
    const opts = Object.assign({ statusCode: httpStatus.BAD_REQUEST }, options);
    return res.status(httpStatus.BAD_REQUEST).send(opts);
  },

  /**
   * No Data Found.
   * @param {Object<response>} res response object of express.js
   * @param {Object} options options.
   * @param {String} options.message message to be pass.
   * @param {Any}   options.data data be send.
   */

  noData: (res, options = {}) => {
    const opts = Object.assign({ statusCode: httpStatus.NOT_FOUND }, options);
    return res.status(httpStatus.NOT_FOUND).send(opts);
  },

  access: (res, options = {}) => {
    const opts = Object.assign({ statusCode: httpStatus.FORBIDDEN }, options);
    return res.status(httpStatus.FORBIDDEN).send(opts);
  },

  // /**
  // * successful  NO CONTENT
  // * @param {Object<response>} res response object of express.js
  // * @param {Object} options options.
  // * @param {String} options.message message to be pass.
  // * @param {Any}   options.data data be send.
  // */

  // no_Content: (res, options = {}) => {
  //   const opts = Object.assign({ statusCode: httpStatus.NO_CONTENT }, options);
  //   return res.status(httpStatus['204']).send(opts);
  // },
};
