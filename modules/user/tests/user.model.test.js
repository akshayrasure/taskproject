import chai, { expect } from 'chai';
import sinon from 'sinon';
import userModel from '../user.model';

chai.config.includeStack = true;

let sandbox;
beforeEach(() => {
  sandbox = sinon.sandbox.create();
});

afterEach(() => {
  sandbox.restore();
});

describe('### user module **user.model.js**', () => {
  it('model should have add function', () => {
    expect(userModel.add).to.be.a('function');
  });
  it('add function should call create method internally', (done) => {
    const userInfo = { name: 'bhavesh', mobile: '9967520490', role: 'admin', createdBy: '5a91062a356bc61114e6a471', credentials: { username: 'bhavesh@gmail.com', password: '123456789' } };
    sandbox.stub(userModel, 'create').resolves(userInfo);
    userModel.add(userInfo)
      .then((result) => {
        expect(result).to.be.deep.equal(userInfo);
        done();
      });
  });
  it('model should have add otp function', () => {
    expect(userModel.addOtp).to.be.a('function');
  });
  // it('add function should call create method internally', (done) => {
  //   const otp=123456;
  //   const userInfo = { name: 'bhavesh', mobile: '9967520490', role: 'admin', otp:12345, createdBy: '5a91062a356bc61114e6a471', credentials: { username: 'bhavesh@gmail.com', password: '123456789' } };
  //   sandbox.stub(userModel, 'create').resolves(userInfo);
  //   userModel.addOtp({ _id: userInfo._id }, { otp: otp })
  //     .then((result) => {
  //       expect(result).to.be.deep.equal(userInfo);
  //       done();
  //     });
  // });
  it('model should have findUserByIdUpdate function', () => {
    expect(userModel.findUserByIdUpdate).to.be.a('function');
  });
  // it('findUserByIdUpdate function should call findByIdAndUpdate method internally', (done) => {
  //   const userInfo = { _id: '5a91212fc26ab6217c729611', name: 'bhavesh', mobile: '9967520490', role: 'admin', createdBy: '5a91062a356bc61114e6a471', credentials: { username: 'bhavesh@gmail.com', password: '123456789' } };
  //   const UpdateuserInfo = { name: 'ajay', mobile: '9967520490', role: 'admin', createdBy: '5a91062a356bc61114e6a471', credentials: { username: 'bhavesh@gmail.com' } };
  //   sandbox.stub(userModel, 'create').resolves(userInfo);
  //   userModel.findUserByIdUpdate({ _id: userInfo._id }, UpdateuserInfo)
  //     .then((result) => {
  //       console.log(result);
  //       expect(result).to.be.deep.equal(UpdateuserInfo);
  //       done();
  //     });
  // });
  it('model should have findUserByIdRemove function', () => {
    expect(userModel.findUserByIdRemove).to.be.a('function');
  });
  // it('findUserByIdUpdate function should call findByIdAndUpdate method internally', (done) => {
  //   const userInfo = { _id: '5a91212fc26ab6217c729611', name: 'bhavesh', mobile: '9967520490', role: 'admin', createdBy: '5a91062a356bc61114e6a471', credentials: { username: 'bhavesh@gmail.com', password: '123456789' } };
  //   sandbox.stub(userModel, 'create').resolves(userInfo);
  //   userModel.findUserByIdRemove(userInfo._id)
  //     .then((result) => {
  //       console.log(result);
  //       expect(result).to.be.deep.equal(userInfo);
  //       done();
  //     });
  // });
  
  it('add function should throw boom error', (done) => {
    const userInfo = { name: 'bhavesh', mobile: '9967520490', role: 'admin', createdBy: '5a91062a356bc61114e6a471', credentials: { username: 'bhavesh@gmail.com', password: '123456789' } };
    sandbox.stub(userModel, 'create').rejects(new Error('some mongo error'));
    userModel.add(userInfo)
      .catch((error) => {
        expect(error.isBoom).to.be.eql(true);
        done();
      });
  });
  it('model should have getDetails function', () => {
    expect(userModel.getDetails).to.be.a('function');
  });
  it('getDetails function should call get method internally', (done) => {
    const userInfo = { name: 'bhavesh', mobile: '9967520490', role: 'admin', createdBy: '5a91062a356bc61114e6a471', credentials: { username: 'bhavesh@gmail.com', password: '123456789' } };
    sandbox.stub(userModel, 'getDetails').resolves(userInfo);
    userModel.getDetails()
      .then((result) => {
        expect(result).to.be.deep.equal(userInfo);
        done();
      });
  });
  it('model should have getDetails function', () => {
    expect(userModel.findUser).to.be.a('function');
  });
  it('FInd User function should call get method internally', (done) => {
    const userInfo = { name: 'bhavesh', mobile: '9967520490', role: 'admin', createdBy: '5a91062a356bc61114e6a471', credentials: { username: 'bhavesh@gmail.com', password: '123456789' } };
    sandbox.stub(userModel, 'findUser').resolves(userInfo);
    userModel.findUser(userInfo.mobile)
      .then((result) => {
        expect(result).to.be.deep.equal(userInfo);
        done();
      });
  });
  it('model should have findUserById function', () => {
    expect(userModel.findUserById).to.be.a('function');
  });
  it('findUserById function should call get method internally', (done) => {
    const userInfo = { _id: '5a91212fc26ab6217c729611', name: 'bhavesh', mobile: '9967520490', role: 'admin', createdBy: '5a91062a356bc61114e6a471', credentials: { username: 'bhavesh@gmail.com', password: '123456789' } };
    sandbox.stub(userModel, 'findUserById').resolves(userInfo);
    userModel.findUserById(userInfo._id)
      .then((result) => {
        expect(result).to.be.deep.equal(userInfo);
        done();
      });
  });
  it('model should have findUserById function', () => {
    expect(userModel.findUserById).to.be.a('function');
  });
  it('findUserById function should call get method internally', (done) => {
    const userInfo = { _id: '5a91212fc26ab6217c729611', name: 'bhavesh', mobile: '9967520490', role: 'admin', createdBy: '5a91062a356bc61114e6a471', credentials: { username: 'bhavesh@gmail.com', password: '123456789' } };
    sandbox.stub(userModel, 'findUserById').resolves(userInfo);
    userModel.findUserById(userInfo._id)
      .then((result) => {
        expect(result).to.be.deep.equal(userInfo);
        done();
      });
  });
});


// describe('### user module **user.model.js**', () => {
//   it('model should have findUserByIdUpdate function', () => {
//     expect(userModel.getDetails).to.be.a('function');
//   });
//   it('findUserByIdUpdate function should call get method internally', (done) => {
//     const userInfo = { _id: '5a91212fc26ab6217c729611', name: 'bhavesh', mobile: '9967520490', role: 'admin' };
//     const body = { _id: '5a91212fc26ab6217c729611', name: 'suresh', mobile: '9967520490', role: 'admin' };
//     sandbox.stub(userModel, 'findUserByIdUpdate').resolves(userInfo);
//     userModel.findUserByIdUpdate(userInfo._id, body)
//       .then((result) => {
//         sandbox.stub(userModel, 'findUserByIdUpdate').resolves(body);
//         console.log(result);
//         expect(result).to.be.deep.equal(body);
//         done();
//       });
//   });
// });