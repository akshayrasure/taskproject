import chai, { expect } from 'chai';
import validation from '../user.validation';

chai.config.includeStack = true;

describe('### user module **user.validation.js**', () => {
  it('should has add property with body', () => {
    expect(validation).to.have.keys('add');
    expect(validation.add).to.have.keys('body');
    expect(validation.add.body).to.have.keys('name', 'mobile', 'role', 'otp');
    // , 'credentials'
  });
});
