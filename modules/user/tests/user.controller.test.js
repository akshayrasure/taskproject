import chai, { expect } from 'chai';
import sinon from 'sinon';
import proxyquire from 'proxyquire';
import userModel from '../user.model';

// stubbing user dependency from user.controller.
const userController = proxyquire('../user.controller', { user: userModel });


chai.config.includeStack = true;

let sandbox;
beforeEach(() => {
  sandbox = sinon.sandbox.create();
});

afterEach(() => {
  sandbox.restore();
});

describe('### user controller **user.controller.js**', () => {
  it('should have adduser property', () => {
    expect(userController).to.be.a('object');
    expect(userController.add).to.be.a('array');
  });
  it('should have get property', () => {
    expect(userController).to.be.a('object');
    expect(userController.get).to.be.a('array');
  });
  it('should have getById property', () => {
    expect(userController).to.be.a('object');
    expect(userController.getById).to.be.a('array');
  });
  it('should have updateById property', () => {
    expect(userController).to.be.a('object');
    expect(userController.updateById).to.be.a('array');
  });
  it('should have removeById property', () => {
    expect(userController).to.be.a('object');
    expect(userController.removeById).to.be.a('array');
  });
});