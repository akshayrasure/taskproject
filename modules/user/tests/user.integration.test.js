import mongoose from 'mongoose';
import jwt from 'jwt-simple';
import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import chai, { expect } from 'chai';
import app from '../../../index';
import config from '../../../config/database';
import User from '../user.model';
// import userFixture from '../helpers/user.fixtures';
chai.config.includeStack = true;

// after((done) => {
//     // required because https://github.com/Automattic/mongoose/issues/1251#issuecomment-65793092
//   mongoose.models = {};
//   mongoose.modelSchemas = {};
//   mongoose.connection.db.dropDatabase();
//   mongoose.connection.close();
//   done();
// });
after((done) => {
  mongoose.models = {};
  mongoose.modelSchemas = {};
  mongoose.connection.db.dropDatabase();
  mongoose.connection.close();
  done();
});
const userData = {
  // _id: '5aa10a6b750f870b7a25f846',
  name: 'bhavesh',
  mobile: '9967520490',
  role: 'admin',
  createdBy: '5ab0d48271402e317a64419a',
  credentials: {
    password: '123456789',
    username: 'abc@fortunekit.com',
  },
};

let token;
let id;

// inserting data from fixtures into database.
beforeEach((done) => {
  token = jwt.encode(userData, config.secret);
  // done();
  User.collection.insert(userData, done);
});

  // emptying database
afterEach((done) => {
  User.remove({}, () => {
    done();
  });
});

// _id: '5ab0d48271402e317a64419b',
describe('## User APIs (integration test)', () => {
  const user = {
    name: 'bhavesh',
    mobile: '9967520491',
    role: 'user',
  };
// post
  describe('# POST /api/user', () => {
  //  console.log(userData, user, token);
    it('should not create a user as token not provided', (done) => {
      request(app)
        .post('/api/user')
        .send(user)
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });
    it('should not create a user bad request', (done) => {
      const user1 = {
        name: 'bhavesh',
        mobile: '9967520491',
        role: 'admina',
        createdBy: '5ab0d48271402e317a64419a',
        // credentials: {
        // },
      };
      request(app)
        .post('/api/user')
        .set('Authorization', `jwt ${token}`)
        .send(user1)
        .expect(httpStatus.BAD_REQUEST)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });
    it('should  create a user', (done) => {
      request(app)
        .post('/api/user')
        .set('Authorization', `jwt ${token}`)
        .send(user)
        .expect(httpStatus.CREATED)
        .then((res) => {
          expect(res.body);
          expect(res.body.data).to.have.property('name');
          expect(res.body.data).to.have.property('mobile');
          expect(res.body.data).to.have.property('role');
          expect(res.body.data.name).to.equal(user.name);
          expect(res.body.data.mobile).to.equal(user.mobile);
          expect(res.body.data.role).to.equal(user.role);
          // expect(res.body.data.createdBy).to.equal(user.createdBy);
          done();
        })
        .catch(done);
    });
  });
// get list
  describe('# GET /api/user', () => {
    it('should not get user list as token not provided', (done) => {
      request(app)
        .get('/api/user')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should get user list', (done) => {
      request(app)
        .get('/api/user')
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          id = res.body.data[0]._id;
          expect(res.body).to.be.an('object');
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal('user found');
          expect(res.body.data).to.be.an('array');
          expect(res.body.data[0]).to.have.property('name');
          expect(res.body.data[0]).to.have.property('mobile');
          expect(res.body.data[0]).to.have.property('role');
          expect(res.body.data[0].name).to.equal('bhavesh');
          expect(res.body.data[0].mobile).to.equal('9967520490');
          expect(res.body.data[0].role).to.equal('admin');
          done();
        })
        .catch(done);
    });
  });
// get by id
  describe('# GET BY ID /api/user/:id', () => {
    it('should not get user as token not provided', (done) => {
      request(app)
        .get('/api/user/4ed2b809d7446b9a0e000023')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });

    it('should get user by id', (done) => {
      request(app)
        .get(`/api/user/${id}`)
        .set('Authorization', `jwt ${token}`)
        .expect(httpStatus.OK)
        .then((res) => {
          console.log('body is :==============>', res.body);
          expect(res.body).to.be.an('object');
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal('user found');
          expect(res.body.data).to.have.property('name');
          expect(res.body.data).to.have.property('mobile');
          expect(res.body.data).to.have.property('role');
          expect(res.body.data.name).to.equal('bhavesh');
          expect(res.body.data.mobile).to.equal('9967520490');
          expect(res.body.data.role).to.equal('admin');
          done();
        })
        .catch(done);
    });
  });
// update
  describe('# UPDATE BY ID /api/user/:id', () => {
    it('should not update user as token not provided', (done) => {
      request(app)
        .put('/api/user/4ed2b809d7446b9a0e000023')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });
    it('should get user by id & update', (done) => {
      user.name = 'ajay';
      request(app)
      .put(`/api/user/${id}`)
        .set('Authorization', `jwt ${token}`)
        .send(user)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('object');
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal('user updated');
          expect(res.body.data).to.have.property('name');
          expect(res.body.data).to.have.property('mobile');
          // expect(res.body.data).to.have.property('role');
          // expect(res.body.data).to.have.property('credentials');
          // expect(res.body.data.name).to.equal('ajay');
          // expect(res.body.data.mobile).to.equal('9967520491');
          // expect(res.body.data.role).to.equal('user');
          done();
        })
        .catch(done);
    });
  });
// delete
  describe('# DELETE BY ID /api/user/:id', () => {
    it('should not delete user as token not provided', (done) => {
      request(app)
        .delete('/api/user/4ed2b809d7446b9a0e000023')
        .expect(httpStatus.UNAUTHORIZED)
        .then((res) => {
          expect(res.body);
          done();
        })
        .catch(done);
    });
    it('should delete user by id', (done) => {
      request(app)
      .delete(`/api/user/${id}`)
        .set('Authorization', `jwt ${token}`)
        .send(user)
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body).to.be.an('object');
          expect(res.body.statusCode).to.equal(200);
          expect(res.body.message).to.equal('user delete');
          expect(res.body.data).to.have.property('name');
          expect(res.body.data).to.have.property('mobile');
          expect(res.body.data).to.have.property('role');
          expect(res.body.data).to.have.property('credentials');
          expect(res.body.data.name).to.equal('bhavesh');
          expect(res.body.data.mobile).to.equal('9967520490');
          expect(res.body.data.role).to.equal('admin');
          done();
        })
        .catch(done);
    });
  });
});
