import chai, { expect } from 'chai';
import sinon from 'sinon';
import { mockRes } from 'sinon-express-mock';
import response from '../user.response';


chai.config.includeStack = true;

describe('### user module **user.response.js**', () => {
  it('should have ERROR,ok and created property', () => {
    expect(response).to.have.keys('ERROR', 'ok', 'created', 'badrequest', 'noData', 'access');
  });

  describe('### response.ERROR ', () => {
    it('ERROR should be an object', () => {
      expect(response.ERROR).to.be.an('object');
    });
    it('ERROR.serverError should return instance of boom', (done) => {
      response.ERROR.serverError(new Error('this is the error'))
      .catch((error) => {
        expect(error.isBoom).to.be.eql(true);
        done();
      });
    });
  });

  it('ok should send statusCode 200 with provided details', () => {
    const send = sinon.spy();
    let mockResponse = {
      status: () => Object.assign({
        send,
      }),
    }; // mocking res.status(200).send() and spying on send method.

    mockResponse = mockRes(mockResponse);

    // now calling ok method with mocked response.
    response.ok(mockResponse, { message: 'this is message', data: 'this is data' });

    sinon.assert.calledWith(send, { statusCode: 200, message: 'this is message', data: 'this is data' });
  });

  it('created should send statusCode 201 with provided details', () => {
    const send = sinon.spy();
    let mockResponse = {
      status: () => Object.assign({
        send,
      }),
    }; // mocking res.status(200).send() and spying on send method.

    mockResponse = mockRes(mockResponse);

    // now calling created method with mocked response.
    response.created(mockResponse, { message: 'this is message', data: 'this is data' });

    sinon.assert.calledWith(send, { statusCode: 201, message: 'this is message', data: 'this is data' });
  });

  it('badrequest should send statusCode 400 with provided details', () => {
    const send = sinon.spy();
    let mockResponse = {
      status: () => Object.assign({
        send,
      }),
    };

    mockResponse = mockRes(mockResponse);

    // now calling finded method with mocked response.
    response.badrequest(mockResponse, { message: 'this is message', data: 'this is data' });

    sinon.assert.calledWith(send, { statusCode: 400, message: 'this is message', data: 'this is data' });
  });
  it('noData should send statusCode 404 with provided details', () => {
    const send = sinon.spy();
    let mockResponse = {
      status: () => Object.assign({
        send,
      }),
    };

    mockResponse = mockRes(mockResponse);

    // now calling noData method with mocked response.
    response.noData(mockResponse, { message: 'this is message', data: 'this is data' });

    sinon.assert.calledWith(send, { statusCode: 404, message: 'this is message', data: 'this is data' });
  });

  it('access should send statusCode 403 with provided details', () => {
    const send = sinon.spy();
    let mockResponse = {
      status: () => Object.assign({
        send,
      }),
    };

    mockResponse = mockRes(mockResponse);

    // now calling noData method with mocked response.
    response.access(mockResponse, { message: 'this is message', data: 'this is data' });

    sinon.assert.calledWith(send, { statusCode: 403, message: 'this is message', data: 'this is data' });
  });
});
