/*eslint-disable*/
/**
 * Define controller function here.
 */

import user from './user.model';
import response from './user.response';

const add = [
  (req, res, next) => {
        user.add(Object.assign(req.body))
          .then(result => response.created(res, { message: 'user added', data: result }))
          .catch(error => next(error));
  },
];
const get = [
  (req, res, next) => {
      user.getDetails()
      .then((result) => {
        if (result.length === 0) {
          response.noData(res, { message: 'no data found', data: result });
        } else {
          response.ok(res, { message: 'user found', data: result });
        }
      })
      .catch(error => next(error));
  },
];
const getById = [
  (req, res, next) => {
      user.findUserById({ _id: req.params._id })
      .then((result) => {
        if ((result === 0) || (result == null)) {
          response.noData(res, { message: 'no data found' });
        } else {
          response.ok(res, { message: 'user found', data: result });
        }
      })
      .catch(error => next(error));
  },
];

const updateById = [
  (req, res, next) => {
        user.findUserByIdUpdate({ _id: req.params._id }, req.body)
        .then((result) => {
          if (result) {
            response.ok(res, { message: 'user data updated', data: result });
          } else {
            response.noData(res, { message: 'no data found' });
          }
        })
          .catch(error => next(error));
  },
];
const removeById = [
  (req, res, next) => {
      user.findUserByIdRemove({ _id: req.params._id })
      .then((result) => {
        if ((result === 0) || (result == null)) {
          response.noData(res, { message: 'no data found' });
        } else {
          response.ok(res, { message: 'user delete', data: result });
        }
      })
      .catch(error => next(error));
  },
];
export default {
  add,
  get,
  getById,
  updateById,
  removeById,
};
