
/**
 * Define every validation requires
 * to perform CRUD on product.
 */


import Joi from 'joi';

// Defining base schema.
const userSchema = {
  name: Joi.string(),
  mobile: Joi.string(),
  role: Joi.string(),
  // createdBy: Joi.string(),
  otp: Joi.string(),
  // credentials: Joi.object().keys({
  //   username: Joi.string(),
  //   password: Joi.string(),
  // }),


};


export default {
  // adding middleware level validation on api endpoint
  add: {
    body: {
      name: userSchema.name.required(),
      mobile: userSchema.mobile.regex(/^[789]\d{9}$/).required(),
      role: userSchema.role.required(),
      // createdBy: userSchema.createdBy.required(),
      otp: Joi.string(),
      // credentials: Joi.object().keys({
      //   username: Joi.string().regex(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/).when('role', { is: 'admin', then: Joi.required(), otherwise: Joi.optional() }),
      //   password: Joi.string(),
      // }).when('role', { is: 'admin', then: Joi.required(), otherwise: Joi.optional() }),
      // username: Joi.any().when('role', { is: 'admin', then: Joi.required(), otherwise: Joi.optional() }),
      // password: Joi.any().when('role', { is: 'admin', then: Joi.required(), otherwise: Joi.optional() }),
    },
  },
};
