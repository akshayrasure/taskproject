/*eslint-disable*/
/**
 * User Schema
 */

import bcrypt from 'bcrypt';
import mongoose from 'mongoose';
import beautifyUnique from 'mongoose-beautiful-unique-validation';
import response from './user.response';

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  mobile: {
    type: String,
    required: true,
    unique: true,
  },
  role: {
    type: String,
  },
  otp: {
    type: String,
  },
  password: {
    type: String,
  }
},
{
  timestamps: true,
  toObject: {
    transform(doc, ret) {
      delete ret.password; // eslint-disable-line no-param-reassign
    },
  },
  toJSON: {
    transform(doc, ret) {
      delete ret.password; // eslint-disable-line no-param-reassign
    },
  },
},
);

userSchema.plugin(beautifyUnique);

userSchema.statics = {

  /**
   * add user
   * @param {Object} user user object.
   * @param {String} user.name user name.
   * @param {String} user.mobile user mobile.
   * @param {String} user.role user role.
   * @returns {Promise<Product, Error>} resolve added product.
   */
  add(data) {
    // console.log("data is :", data);
    return this.create(data)
    .catch(error => response.ERROR.validateError(error));
  },
  /**
  * Get user
  * @returns {Promise<user details, APIError>}
  */
  getDetails() {
    return this.find()
      .catch(error => response.ERROR.serverError(error));
  },
  
  /**
    * Find user by id
    * @param {String} user._id user mobile.
    * @returns {Promise<user details, APIError>}
    */
  findUserById(_id) {
    return this.findById(_id)      
    .catch(error => response.badrequest(error));
  },
  /**
    * Find user by id & update
    * @param {String} user._id user mobile.
    * @returns {Promise<user details, APIError>}
    */ 
  findUserByIdUpdate(_id, body) {
    return this.findByIdAndUpdate(_id, body, { new: true })
      .catch(error => response.ERROR.validateError(error));
  },

  /**
    * Find user by id & Remove
    * @param {String} user._id user mobile.
    * @returns {Promise<user details, APIError>}
    */
  findUserByIdRemove(_id) {
    return this.findByIdAndRemove(_id)
      .catch(error => response.badrequest(error));
  },
/**
    * Add Otp , finde one & update
    * @param {String} user._id user mobile.
    * @returns {Promise<user details, APIError>}
    */
  addOtp(user, otp) {
    return this.findOneAndUpdate({ _id: user._id }, { otp }, { new: true })
    .catch(error => response.badrequest(error));
  },
};


userSchema.pre('save', function (next) {
  var user = this;
  if (this.password){
  if (this.isModified('password') || this.isNew) {
      bcrypt.genSalt(10, function (err, salt) {
        if (err) {
            return next(err);
          }
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err) {
                return next(err);
              }
            user.password = hash;
            next();
          });
      });
    } else {
      return next();
    }
  } else {
    next();
  }

});

userSchema.pre('findOneAndUpdate', function (next) {
  var user = this;
  if (user._update.password) {
    bcrypt.genSalt(10, function (err, salt) {
        if (err) {
            return next(err);
          }
        bcrypt.hash(user._update.password, salt, function (err, hash) {
            if (err) {
                return next(err);
              }
            user._update.password = hash;
            next();
          });
      });
  } else {
    return next();
  }
});

userSchema.methods.comparePassword = function (passw, cb) {
  bcrypt.compare(passw, this.password, function (err, isMatch) {
    if (err) {
        return cb(err);
      }
    cb(null, isMatch);
  });
};


export default mongoose.model('User', userSchema);
