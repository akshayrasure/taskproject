
/* eslint new-cap:0 */

/**
 * import all modules.
 */

import express from 'express';

import user from './user/index';

import smslogin from './sms login/smslogin.index';

import login from './auth/index';


/**
 * defining express route.
 */
const routes = express.Router();

routes.use('/user', user.route);

routes.use('/smslogin', smslogin.route);

routes.use('/login', login.route);


export default {
  /**
   * modules export.
   */
  modules: {
    user,
    smslogin,
    login,
  },
  /**
   * express routes.
   */
  routes,
};
