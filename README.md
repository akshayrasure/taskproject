
# Server-Starter-Kit
> Kick start developing business logic without worry about base project structure with
  testing, code coverage support, eslint config etc.

### Prerequisites
Before start development. Please install.

```
node: >=4.8.0
```
```
npm : >=2.15.11,
```
```
yarn: >=0.20.3
```

### How to do git commit?
First of all add your changes into staging by using
```shell
git add <changedFile.ext>
```

and then 
```shell
yarn commit
```

this would ask few questions like type, scope, short description, detail explanation,
closing issues  of the changes.

![commit window](https://cdn-images-1.medium.com/max/800/1*tB1XTkxryDmMXGcls-qe5Q.png)

